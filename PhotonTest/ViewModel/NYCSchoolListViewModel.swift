//
//  NYCSchoolListViewModel.swift
//  PhotonTest
//
//  Created by Navyasree Janjirala on 7/26/22.
//

import UIKit

typealias NYCSchoolListModel = [[String: String]]

class NYCSchoolListViewModel {
    
    var schoolDetails: [NYCSchoolNamesResponseModel] = [NYCSchoolNamesResponseModel]()
    var reloadTableView: (()->())?
    var showError: (()->())?
    var showLoading: (()->())?
    var hideLoading: (()->())?
    
    private var cellViewModels: [SchoolListCellViewModel] = [SchoolListCellViewModel]() {
        didSet {
            self.reloadTableView?()
        }
    }
    
    func getschoolDetails() {
        showLoading?()
        let config = URLSessionConfiguration.ephemeral
        let urlSession = URLSession(configuration: config)
        let school = NYCSchoolWebservice(urlString: NYCConstants.nycSchoolNamesURLString, urlSession: urlSession)
        school.nycSchoolNames { (response, error) in
            if let schoolDetails = response {
                self.createCell(datas: schoolDetails)
            }
            self.hideLoading?()
        }
    }
    
    var numberOfCells: Int {
        return cellViewModels.count
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> SchoolListCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func createCell(datas: [NYCSchoolNamesResponseModel]){
        self.schoolDetails = datas
        var vms = [SchoolListCellViewModel]()
        for data in datas {
            vms.append(SchoolListCellViewModel(schoolName: data.school_name))
        }
        cellViewModels = vms
    }
}

struct SchoolListCellViewModel {
    let schoolName: String
}

