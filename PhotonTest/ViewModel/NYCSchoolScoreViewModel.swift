//
//  NYCSchoolScoreViewModel.swift
//  PhotonTest
//
//  Created by Navyasree Janjirala on 7/26/22.
//

import UIKit
class NYCSchoolScoreViewModel {
    
    var schoolDetails: [NycScoreModel] = [NycScoreModel]()
    var reloadTableView: (()->())?
    var showError: (()->())?
    var showLoading: (()->())?
    var hideLoading: (()->())?
    
    private var cellViewModels: [ScoreCellViewModel] = [ScoreCellViewModel]() {
        didSet {
            self.reloadTableView?()
        }
    }
    
    func getScoreDetails() {
        showLoading?()
        let config = URLSessionConfiguration.ephemeral
        let urlSession = URLSession(configuration: config)
        let school = NYCSchoolWebservice(urlString: NYCConstants.nycScoreurlString, urlSession: urlSession)
        school.nycScoreDetails { (response, error) in
            if let schoolDetails = response {
                self.createCell(datas: schoolDetails)
            }
            self.hideLoading?()
        }
    }
    
    var numberOfCells: Int {
        return cellViewModels.count
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> ScoreCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func createCell(datas: [NycScoreModel]){
        self.schoolDetails = datas
        var vms = [ScoreCellViewModel]()
        for data in datas {
            vms.append(ScoreCellViewModel(dbn: data.dbn, schoolName: data.schoolName, testTaker: data.numOfSatTestTakers, mathScore: data.satMathAvgScore, readingScore: data.satCriticalReadingAvgScore, writingScore: data.satWritingAvgScore))
        }
        cellViewModels = vms
    }
}

struct ScoreCellViewModel {
    let dbn: String
    let schoolName: String
    let testTaker: String
    let mathScore: String
    let readingScore: String
    let writingScore: String
}
