//
//  SignUpError.swift
//  PhotonTest
//
//  Created by Navyasree Janjirala on 8/8/22.
//

import Foundation

enum NYCError: LocalizedError, Equatable {
    
    case invalidResponseModel
    case invalidRequestURLString
    case failedRequest(description: String)
    
    var errorDescription: String? {
        switch self {
        case .failedRequest(let description):
            return description
        case .invalidResponseModel, .invalidRequestURLString:
            return ""
        }
    }
    
}
