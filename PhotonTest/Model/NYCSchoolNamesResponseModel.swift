//
//  SchoolDetails.swift
//  PhotonTest
//
//  Created by Navyasree Janjirala on 7/26/22.
//

import Foundation

struct NYCSchoolNamesResponseModel: Decodable {
    let dbn: String
    let school_name: String
    let boro: String
    let overview_paragraph: String
    let location: String
    let website: String
    let phone_number: String
}
