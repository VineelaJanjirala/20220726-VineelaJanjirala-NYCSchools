//
//  NYCConstants.swift
//  PhotonTest
//
//  Created by Navyasree Janjirala on 8/8/22.
//

import Foundation

struct NYCConstants {
    static let nycSchoolNamesURLString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let nycScoreurlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}
