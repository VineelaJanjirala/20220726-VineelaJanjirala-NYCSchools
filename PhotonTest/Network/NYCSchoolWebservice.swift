//
//  APIClient.swift
//  PhotonTest
//
//  Created by Navyasree Janjirala on 7/26/22.
//

import Foundation

class NYCSchoolWebservice {
    
    private var urlSession: URLSession
    private var urlString: String
    
    init(urlString: String, urlSession: URLSession = .shared) {
        self.urlString = urlString
        self.urlSession = urlSession
    }
    
    func nycSchoolNames( completionHandler: @escaping ([NYCSchoolNamesResponseModel]?, NYCError?) -> Void) {
        guard let url = URL(string: urlString) else {
            completionHandler(nil, NYCError.invalidRequestURLString)
            return
        }
        let urlSession = urlSession.dataTask(with: url) { (data, response, error) in
            if let requestError = error {
                completionHandler(nil, NYCError.failedRequest(description: requestError.localizedDescription))
                return
            }
            if let data=data, let nycSchoolNamesResponseModel = try? JSONDecoder().decode([NYCSchoolNamesResponseModel].self, from: data) {
                completionHandler(nycSchoolNamesResponseModel, nil)
            } else {
                completionHandler(nil, NYCError.invalidResponseModel)
            }
        }
        urlSession.resume()
    }
    
    
    func nycScoreDetails( completionHandler: @escaping ([NycScoreModel]?, NYCError?) -> Void) {
        guard let url = URL(string: urlString) else {
            completionHandler(nil, NYCError.invalidRequestURLString)
            return
        }
        let urlSession = urlSession.dataTask(with: url) { (data, response, error) in
            if let requestError = error {
                completionHandler(nil, NYCError.failedRequest(description: requestError.localizedDescription))
                return
            }
            if let data=data, let nycScoreResponseModel = try? JSONDecoder().decode([NycScoreModel].self, from: data) {
                completionHandler(nycScoreResponseModel, nil)
            } else {
                completionHandler(nil, NYCError.invalidResponseModel)
            }
        }
        urlSession.resume()
    }
}

