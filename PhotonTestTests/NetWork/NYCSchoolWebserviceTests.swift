//
//  NYCSchoolWebserviceTest.swift
//  PhotonTestTests
//
//  Created by Navyasree Janjirala on 8/8/22.
//

import XCTest
@testable import PhotonTest

class NYCSchoolWebserviceTests: XCTestCase {
    
    var sut:NYCSchoolWebservice!

    override func setUpWithError() throws {
        
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [MockURLProtocol.self]
        let urlSession = URLSession(configuration: config)
        sut = NYCSchoolWebservice(urlString: NYCConstants.nycSchoolNamesURLString, urlSession: urlSession)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
    }
    
    func testNYCSchoolWebservice_WhenEmptyURLStringProvided_ReturnsError() {
        // Arrange
        let expectation = self.expectation(description: "An empty request URL string expectation")
        
        sut = NYCSchoolWebservice(urlString: "")
        // Act

        sut.nycSchoolNames { (response, error) in
            //Assert
            XCTAssertEqual(error, NYCError.invalidRequestURLString, "The nycSchoolNames() method did not return an expected error for an invalidRequestURLString error")
            XCTAssertNil(response, "When an invalidRequestURLString takes place, the response model must be nil")
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 2)
    }
    
    func testNYCSchoolWebservice_WhenURLRequestFails_ReturnsErrorMessageDescription() {
        
        // Arrange
        let expectation = self.expectation(description: "A failed Request expectation")
        let errorDescription = "A localized description of an error"
        MockURLProtocol.error = NYCError.failedRequest(description:errorDescription)
        
        // Act
        sut.nycSchoolNames { (response, error) in
            // Assert
           // XCTAssertEqual(error, SignupError.failedRequest(description:errorDescription), "The nycSchoolNames() method did not return an expecter error for the Failed Request")
            // XCTAssertEqual(error?.localizedDescription, errorDescription)
            XCTAssertNil(response, "When an invalidRequest takes place, the response model must be nil")

            expectation.fulfill()
        }
        
        self.wait(for: [expectation], timeout: 2)
        
    }
    
    func testNYCSchoolWebservice_WhenGivenSuccessfullResponse_ReturnsSuccess() {
        
        // Arrange
        let jsonString = "[{\"dbn\":\"02M260\",\"school_name\":\"Clinton School Writers \",\"boro\":\"M\",\"overview_paragraph\":\"Test Paragraph\"}]"
        MockURLProtocol.stubResponseData =  jsonString.data(using: .utf8)
        
        let expectation = self.expectation(description: "nycSchoolNames Web Service Response Expectation")
        
        // Act
        sut.nycSchoolNames { (response, error) in
            // Assert
            //"{\"status\":\"ok\"}"
            XCTAssertEqual(response?[0].dbn, "02M260")
            expectation.fulfill()
        }
        self.wait(for: [expectation], timeout: 5)
    }
    
    func testNYCSchoolWebservice_WhenReceivedDifferentJSONResponse_ErrorTookPlace() {
        // Arrange
        let jsonString = "{\"path\":\"/users\", \"error\":\"Internal Server Error\"}"
        MockURLProtocol.stubResponseData =  jsonString.data(using: .utf8)
        
        let expectation = self.expectation(description: "nycSchoolNames() method expectation for a response that contains a different JSON structure")
        
        // Act
        sut.nycSchoolNames { (response, error) in
            // Assert
            XCTAssertNil(response, "The response model for a request containing unknown JSON response, should have been nil")
            XCTAssertEqual(error, NYCError.invalidResponseModel, "The nycSchoolNames() method did not return expected error")
            expectation.fulfill()
        }
        
        self.wait(for: [expectation], timeout: 5)
    }
}
